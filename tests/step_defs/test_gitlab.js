import { Before, Given, Then, And } from 'cypress-cucumber-preprocessor/steps';

const GITLAB_URI = 'https://gitlab.com/explore?&name=whitehouse&sort=latest_activity_desc';

Given('that user has entered to Gitlab start page', () => {
  cy.visit(GITLAB_URI);
});

And('user has clicked tab named "Explore projects"', () => {});

Given('user type a {substring} to search element', number => {});

Then('next <number> of projects was rendered on page', () => {});
