Feature: searching Gitlab projects
  User can get list of Gitlab projects
  Which can be found by search substring

  Background:
    Given that user has entered to Gitlab start page
    And user has clicked tab named "Explore projects"

  Scenario Outline: got not empty list of projects
      Given user type a <substring> to search element
      Then next <number> of projects was rendered on page

      Examples:
      | substring            | number |
      | kremlin              | 2      |
      | whitehouse           | 8      |
