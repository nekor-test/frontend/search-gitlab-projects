module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',

  verbose: true,
  collectCoverage: true,
  collectCoverageFrom: ['src/**/*.{js,vue}', '!**/node_modules/**'],
  coverageReporters: ['html', 'text-summary'],
  setupFiles: ['./tests/setup.js'],
  testMatch: ['**/tests/unit/**/*.(spec|steps).[jt]s?(x)', '**/__tests__/*?steps.[jt]s?(x)'],
};
